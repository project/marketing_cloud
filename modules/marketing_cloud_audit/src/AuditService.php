<?php

namespace Drupal\marketing_cloud_audit;

use Drupal\marketing_cloud\MarketingCloudService;

/**
 * Class AuditService.
 *
 * For all of the API service calls, a correct JSON data payload is expected.
 * This is then validated against the JSON Schema. This approach minimises
 * any short-term issues with changes in the SF API, provides a sanitized
 * interface to send API calls and leaves flexibility for any modules that
 * want to use this as a base-module.
 *
 * @package Drupal\marketing_cloud
 */
class AuditService extends MarketingCloudService {

  /**
   * The machine name of the sub-module.
   *
   * @var string
   */
  private $moduleName = 'marketing_cloud_audit';

  /**
   * Retrieves logged Audit Trail audit events for the current account and its children. Logins are audited at the enterprise level.
   *
   * @param array $params
   *   URL filter params. Permissible keys:
   *     $page number
   *       Page number to return from the paged results.
   *     $pagesize number
   *       Number of results per page to return.
   *     $orderBy string
   *       Determines which property to use for sorting and the direction in which to sort the data.
   *     startdate string
   *       Start date of the date range to search for security events.
   *     enddate string
   *       End date of the date range to search for security events.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/getAuditEvents.htm
   */
  public function getAuditEvents(array $params = NULL) {
    $machineName = 'get_audit_events';
    return $this->apiCall($this->moduleName, $machineName, new \stdClass(), [], $params);
  }

  /**
   * Retrieves logged Audit Trail security events for the authenticated user’s account and its children. Logins are audited at the enterprise level.
   *
   * @param array $params
   *   URL filter params. Permissible keys:
   *     $page number
   *       Page number to return from the paged results.
   *     $pagesize number
   *       Number of results per page to return.
   *     $orderBy string
   *       Determines which property to use for sorting and the direction in which to sort the data.
   *     startdate string
   *       Start date of the date range to search for security events.
   *     enddate string
   *       End date of the date range to search for security events.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/getSecurityEvents.htm
   */
  public function getSecurityEvents(array $params = NULL) {
    $machineName = 'get_security_events';
    return $this->apiCall($this->moduleName, $machineName, new \stdClass(), [], $params);
  }

}
