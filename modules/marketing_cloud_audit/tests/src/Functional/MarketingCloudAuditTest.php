<?php

namespace Drupal\Tests\marketing_cloud_audit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the base marketing_cloud_audit module.
 *
 * @group marketing_cloud
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MarketingCloudAuditTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['marketing_cloud', 'marketing_cloud_audit'];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Address service.
   *
   * @var \Drupal\marketing_cloud_audit\AuditService
   */
  protected $service;

  /**
   * Config service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $moduleConfig;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user.
    $this->adminUser = $this->drupalCreateUser(['administer_marketing_cloud']);
    $this->drupalLogin($this->adminUser);
    // Set module config.
    $this->config('marketing_cloud.settings')
      ->set('client_id', 'testingid')
      ->set('client_secret', 'testingsecret')
      ->set('validate_json', TRUE)
      ->set('do_not_send', TRUE)
      ->save();
    // Create service.
    $this->service = \Drupal::service('marketing_cloud_audit.service');
    // Get marketing_cloud_assets config object.
    $this->moduleConfig = \Drupal::config('marketing_cloud_audit.settings');
  }

  /**
   * Tests the services and schemas for marketing_cloud_audit.
   */
  public function testDefinitions() {
    // Test schema.
    $this->validateDefinition('get_audit_events');
    // Test getAuditEvents against expected inputs.
    $result = $this->service
      ->getAuditEvents($this->getParamData());
    $this->assertEquals('https://www.exacttargetapis.com/data/v1/audit/auditEvents?$page=2&$pagesize=10&$orderBy=createdDate&startdate=-60&enddate=-30',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals('{}', $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('get', $result['method'], 'Unexpected URL.');
    // Test getSecurityEvents against expected inputs.
    $this->validateDefinition('get_security_events');
    $result = $this->service
      ->getSecurityEvents($this->getParamData());
    $this->assertEquals('https://www.exacttargetapis.com/data/v1/audit/securityEvents?$page=2&$pagesize=10&$orderBy=createdDate&startdate=-60&enddate=-30',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals('{}', $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('get', $result['method'], 'Unexpected URL.');
  }

  /**
   * Test that the Json-Schema is valid, and that the API method id correct.
   *
   * @param string $machineName
   *   The machine name for the api call definition.
   */
  protected function validateDefinition($machineName) {
    // Validate schema.
    $schema = $this->moduleConfig->get("definitions.$machineName.schema");
    $this->assertEquals($schema, '', "json schema for $machineName should be an empty string.");
  }

  /**
   * Get parameter data for tests.
   *
   * @return array
   *   Array of parameter data.
   */
  private function getParamData() {
    return [
      '$page' => 2,
      '$pagesize' => 10,
      '$orderBy' => 'createdDate',
      'startdate' => -60,
      'enddate' => -30,
    ];
  }

}
