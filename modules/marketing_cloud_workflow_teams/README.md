MARKETING CLOUD WORKFLOW TEAMS API
===========================


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Service functions
 * Requirements
 * Installation & Configuration


INTRODUCTION
------------

This module enables the Workflow Teams API in the Marketing Cloud as a service.

For details on individual API calls and the Marketing Cloud REST API, please
visit
https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/routes.htm


SERVICE FUNCTIONS
-----------------

| Name                | Function                   |
| ------------------- | -------------------------- |
| Get Active Workflow Teams | getActiveWorkflowTeams($objectType, $params)    |
| State Transition | stateTransition($workflowItemId, $json) |
| Create User Role | createUserRole($workflowRoleInstanceId, $workflowItemId, $json) |
| Delete User Role | deleteUserRole($workflowRoleInstanceId, $workflowItemId, $userId) |

REQUIREMENTS
------------

 * marketing_cloud


INSTALLATION & CONFIGURATION
----------------------------

This module will add a tab to admin > config > marketing cloud. here you can
edit the individual rest call definitions.

Please see the
[community documentation pages](https://www.drupal.org/docs/8/modules/marketing-cloud)
for information on installation and configuration.
