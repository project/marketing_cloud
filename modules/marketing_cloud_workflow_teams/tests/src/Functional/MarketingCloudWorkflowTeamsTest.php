<?php

namespace Drupal\Tests\marketing_cloud_audit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the base marketing_cloud_workflow_teams module.
 *
 * @group marketing_cloud
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MarketingCloudWorkflowTeamsTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['marketing_cloud', 'marketing_cloud_workflow_teams'];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Workflow teams service.
   *
   * @var \Drupal\marketing_cloud_workflow_teams\WorkflowTeamsService
   */
  protected $service;

  /**
   * Config service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $moduleConfig;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user.
    $this->adminUser = $this->drupalCreateUser(['administer_marketing_cloud']);
    $this->drupalLogin($this->adminUser);
    // Set module config.
    $this->config('marketing_cloud.settings')
      ->set('client_id', 'testingid')
      ->set('client_secret', 'testingsecret')
      ->set('validate_json', TRUE)
      ->set('do_not_send', TRUE)
      ->save();
    // Create service.
    $this->service = \Drupal::service('marketing_cloud_workflow_teams.service');
    // Get marketing_cloud_assets config object.
    $this->moduleConfig = \Drupal::config('marketing_cloud_workflow_teams.settings');
  }

  /**
   * Tests the services and schemas for marketing_cloud_audit.
   */
  public function testDefinitions() {
    // Test get_active_workflow_teams().
    $this->validateDefinitionIsEmpty('get_active_workflow_teams');
    // Test getActiveWorkflowTeams against expected inputs.
    $result = $this->service
      ->getActiveWorkflowTeams(
        '@all',
        ['assignee' => '@current', 'extra' => 'roleusers']
      );
    $this->assertEquals('https://www.exacttargetapis.com/hub/v1/workflowteams/@all?assignee=@current&extra=roleusers',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals('{}', $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('get', $result['method'], 'Unexpected method.');

    // Test state_transition().
    $this->validateDefinitionNotEmpty('state_transition');
    // Test getStateTransitionData against expected inputs.
    $json = $this->getStateTransitionData();
    $result = $this->service
      ->stateTransition('workflowitemid', $json);
    $this->assertEquals('https://www.exacttargetapis.com/hub/v1/workflowitems/workflowitemid/transitions',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals(json_encode($json), $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('post', $result['method'], 'Unexpected method.');

    // Test getActiveWorkflowTeams().
    $this->validateDefinitionNotEmpty('create_user_role');
    // Test getActiveWorkflowTeams against expected inputs.
    $json = $this->getActiveWorkflowTeamsData();
    $result = $this->service
      ->createUserRole('jyfiughjk', 'jgfjhf', $json);
    $this->assertEquals('https://www.exacttargetapis.com/hub/v1/workflowitems/jgfjhf/roles/jyfiughjk',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals(json_encode($json), $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('post', $result['method'], 'Unexpected method.');

    // Test deleteUserRole().
    $this->validateDefinitionIsEmpty('delete_user_role');
    // Test deleteUserRole against expected inputs.
    $result = $this->service
      ->deleteUserRole('jyfiughjk', 'jgfjhf', 'li76syred');
    $this->assertEquals('https://www.exacttargetapis.com/hub/v1/workflowitems/jgfjhf/roles/jyfiughjk/users/li76syred',
      $result['url'],
      'Unexpected URL.');
    $this->assertEquals('{}', $result['data'], 'Unexpected JSON data.');
    $this->assertEquals('delete', $result['method'], 'Unexpected method.');
  }

  /**
   * Test that the Json-Schema is empty, and that the API method id correct.
   *
   * @param string $machineName
   *   The machine name for the api call definition.
   */
  protected function validateDefinitionIsEmpty($machineName) {
    // Validate schema.
    $schema = $this->moduleConfig->get("definitions.$machineName.schema");
    $this->assertEquals($schema, '', "json schema for $machineName should be an empty string.");
  }

  /**
   * Test that the Json-Schema is not empty, and that the API method id correct.
   *
   * @param string $machineName
   *   The machine name for the api call definition.
   */
  protected function validateDefinitionNotEmpty($machineName) {
    // Validate schema.
    $schema = $this->moduleConfig->get("definitions.$machineName.schema");
    $this->assertNotEquals($schema, '', "json schema for $machineName should be an empty string.");
  }

  /**
   * Return test data for state transition.
   */
  private function getStateTransitionData() {
    return [
      'transitionid' => '865rkv',
      'overrideNotificationId' => 'fsthjerhn',
    ];
  }

  /**
   * Return test data for active workflow.
   */
  private function getActiveWorkflowTeamsData() {
    return [
      'userId' => 'jgfd540ilk',
      'userName' => 'ghji98765rfgh',
    ];
  }

}
