<?php

namespace Drupal\marketing_cloud_workflow_teams;

use Drupal\marketing_cloud\MarketingCloudService;

/**
 * Class WorkflowTeamsService.
 *
 * For all of the API service calls, a correct JSON data payload is expected.
 * This is then validated against the JSON Schema. This approach minimises
 * any short-term issues with changes in the SF API, provides a sanitized
 * interface to send API calls and leaves flexibility for any modules that
 * want to use this as a base-module.
 *
 * @package Drupal\marketing_cloud
 */
class WorkflowTeamsService extends MarketingCloudService {

  /**
   * The machine name of the sub-module.
   *
   * @var string
   */
  private $moduleName = 'marketing_cloud_workflow_teams';

  /**
   * Retrieves a list of active workflow teams. Depending on the query string parameters, the result includes one or both filtered results: role user information for each workflow item or workflow items to which the current user is assigned.
   *
   * @param string $objectType
   *   Specify the workflow object type to retrieve or use (@all to retrieve all workflow object types).
   * @param array $params
   *   URL filter params. Permissible keys:
   *     assignee string
   *       If assignee=@current, this resource retrieves workflow teams that the current user is assigned to.
   *     extra string
   *       If extra=roleusers, then each item in the response includes role user information as a collection.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/routes.htm
   */
  public function getActiveWorkflowTeams($objectType, array $params = []) {
    $machineName = 'get_active_workflow_teams';
    return $this->apiCall(
      $this->moduleName,
      $machineName,
      new \stdClass(),
      ['[objectType]' => $objectType],
      $params
    );
  }

  /**
   * Transitions the workflow item associated to an approval item from one state to another. For example, a workflow item in Draft state moves to Submitted via the Submit transition.
   *
   * @param string $workflowItemId
   *   Id of the workflow item.
   * @param array|object|string $json
   *   The JSON payload.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/routes.htm
   */
  public function stateTransition($workflowItemId, $json) {
    $machineName = 'state_transition';
    return $this->apiCall($this->moduleName, $machineName, $json, ['[workflowItemId]' => $workflowItemId], []);
  }

  /**
   * Creates a user role to assign to a workflow role.
   *
   * @param string $workflowRoleInstanceId
   *   Id of the workflow role instance.
   * @param string $workflowItemId
   *   Id of the workflow item.
   * @param array|object|string $json
   *   The JSON payload.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/routes.htm
   */
  public function createUserRole($workflowRoleInstanceId, $workflowItemId, $json) {
    $machineName = 'create_user_role';
    return $this->apiCall(
      $this->moduleName,
      $machineName,
      $json,
      [
        '[workflowRoleInstanceId]' => $workflowRoleInstanceId,
        '[workflowItemId]' => $workflowItemId,
      ],
      []
    );
  }

  /**
   * Removes a user assigned to a workflow role.
   *
   * @param string $workflowRoleInstanceId
   *   Id of the workflow role instance record.
   * @param string $workflowItemId
   *   Id of the workflow item.
   * @param string $userId
   *   Id of the user to be removed.
   *
   * @return array|bool|null
   *   The result of the API call or FALSE on failure.
   *
   * @see https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/routes.htm
   */
  public function deleteUserRole($workflowRoleInstanceId, $workflowItemId, $userId) {
    $machineName = 'delete_user_role';
    return $this->apiCall(
      $this->moduleName,
      $machineName,
      new \stdClass(),
      [
        '[workflowRoleInstanceId]' => $workflowRoleInstanceId,
        '[workflowItemId]' => $workflowItemId,
        '[userId]' => $userId,
      ],
      []
    );
  }

}
