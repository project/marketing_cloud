<?php

namespace Drupal\Tests\marketing_cloud_messages\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the base marketing_cloud_messages module.
 *
 * @group marketing_cloud
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class MarketingCloudMessagesTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['marketing_cloud', 'marketing_cloud_messages'];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Messages service.
   *
   * @var \Drupal\marketing_cloud_messages\MessagesService
   */
  protected $service;

  /**
   * Config service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $moduleConfig;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user.
    $this->adminUser = $this->drupalCreateUser(['administer_marketing_cloud']);
    $this->drupalLogin($this->adminUser);
    // Set module config.
    $this->config('marketing_cloud.settings')
      ->set('client_id', 'testingid')
      ->set('client_secret', 'testingsecret')
      ->set('validate_json', TRUE)
      ->set('do_not_send', TRUE)
      ->save();
    // Create service.
    $this->service = \Drupal::service('marketing_cloud_messages.service');
    // Get marketing_cloud_assets config object.
    $this->moduleConfig = \Drupal::config('marketing_cloud_messages.settings');
  }

  /**
   * Tests the services and schemas for marketing_cloud_address.
   */
  public function testDefinitions() {
    /*
     * Test sendEmail
     */
    $this->validateDefinition('send_email');
    // Validate service against expected inputs.
    $result = $this->service
      ->sendEmail('dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc');
    $this->assertNotFalse($result, 'Unable to parse the input data in sendEmail().');
    $this->assertEquals(
      [
        'url' => 'https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc/send',
        'data' => '{}',
        'method' => 'post',
      ],
      $result, 'Unexpected request data generated by sendEmail()'
    );
    $json = $this->sendEmailJson();
    $result = $this->service
      ->sendEmail('dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc', $json);
    $this->assertNotFalse($result, 'Unable to parse the input data in sendEmail().');
    $this->assertEquals(
      [
        'url' => 'https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc/send',
        'data' => json_encode($json),
        'method' => 'post',
      ],
      $result, 'Unexpected request data generated by sendEmail()'
    );
    // Test against invalid extra index.
    $json['foo'] = 'bar';
    $result = $this->service
      ->sendEmail('dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc', $json);
    $this->assertFalse($result, 'Failed to detect invalid json against the schema in sendEmail()');
    unset($json['foo']);
    // Test against invalid type.
    $from = $json['from'];
    $json['from'] = 'foobar';
    $result = $this->service
      ->sendEmail('dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc', $json);
    $this->assertFalse($result, 'Failed to detect invalid json against the schema in insertJourney()');
    $json['from'] = $from;

    /*
     * Test getEmailDeliveryStatus
     */
    $this->validateDefinition('get_email_delivery_status');
    // Validate service against expected inputs.
    $result = $this->service
      ->getEmailDeliveryStatus('dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc', 'a7038ea5-51b7-4574-ac22-183654378dd2');
    $this->assertNotFalse($result, 'Unable to parse the input data in getEmailDeliveryStatus().');
    $this->assertEquals(
      [
        'url' => 'https://www.exacttargetapis.com/messaging/v1/messageDefinitionSends/dfa5ab87-1b0f-e211-b71b-9c8e9920e9fc/deliveryRecords/a7038ea5-51b7-4574-ac22-183654378dd2',
        'data' => '{}',
        'method' => 'get',
      ],
      $result, 'Unexpected request data generated by getEmailDeliveryStatus()'
    );
  }

  /**
   * Test that the Json-Schema is valid, and that the API method id correct.
   *
   * @param string $machineName
   *   The machine name for the api call definition.
   */
  protected function validateDefinition($machineName) {
    // Validate schema.
    $schema = $this->moduleConfig->get("definitions.$machineName.schema");
    $this->assertNotEmpty($schema, "json schema for $machineName definition is empty.");
    $schema = json_decode($schema);
    $this->assertNotEmpty($schema, "json schema for $machineName definition is invalid json.");
  }

  /**
   * Create a sample payload 2 for testing sendEmail().
   *
   * @return array
   *   Sample payload 2 for the sendEmail() Json.
   */
  private function sendEmailJson() {
    return [
      "from" => [
        "address" => "code@exacttarget.com",
        "name" => "Code@",
      ],
      "to" => [
        "address" => "example@example.com",
        "subscriberKey" => "example@example.com",
        "contactAttributes" => [
          "subscriberAttributes" => [
            "region" => "West",
            "city" => "Indianapolis",
            "state" => "IN",
          ],
        ],
      ],
      "options" => [
        "requestType" => "ASYNC",
      ],
    ];
  }

}
