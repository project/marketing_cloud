<?php

namespace Drupal\marketing_cloud;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class MarketingCloudSession.
 *
 * This class is used to maintain the Marketing Cloud token
 * The public points of entry are:
 *   resetToken()
 *     Clear the existing token - this is used if somehow the token becomes
 *     stuck and cannot be refreshed.
 *   token(FALSE)
 *     Fetch the existing token, or a create a new one if it is stale.
 *
 * @package Drupal\marketing_cloud
 */
class MarketingCloudSession {

  /**
   * Marketing Cloud settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Drupal state API.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * MarketingCloudSession constructor.
   */
  public function __construct() {
    $this->config = \Drupal::configFactory()->get('marketing_cloud.settings');
    $this->state = \Drupal::state();
  }

  /**
   * Reset the stored token.
   */
  public function resetToken() {
    $this->state->set('marketing_cloud_token', FALSE);
    $this->state->set('marketing_cloud_requesting_token', FALSE);
  }

  /**
   * Create and store token for session.
   *
   * @param bool $forceLogin
   *   TRUE = force fetching of a new token.
   *   FALSE = use existing token or fetch a fresh one of stale.
   *
   * @return bool|null
   *   The token or FALSE on failure.
   */
  public function token($forceLogin = FALSE) {
    if ($this->state->get('marketing_cloud_requesting_token') == TRUE) {
      // Wait n seconds to prevent overloading token request with simultaneous
      // requests.
      sleep($this->config->get('requestToken_wait'));
    }

    $token = $this->state->get('marketing_cloud_token');
    $expired = \Drupal::time()->getCurrentTime() >= $this->state->get('marketing_cloud_request_token_expire');
    $recalculate_token = $expired || $forceLogin || empty($token);
    if (!$recalculate_token) {
      return $token;
    }

    // Prevent flooding by setting requesting_token to TRUE.
    $this->state->set('marketing_cloud_requesting_token', TRUE);
    $token = FALSE;

    // Validate required params for token request.
    $token_requisites = TRUE;
    $clientId = $this->config->get('client_id');

    if (empty($clientId)) {
      \Drupal::logger(__METHOD__)->error('Bad config data: %missingData',
        ['%missingData' => 'client_id']
      );
      $token_requisites = FALSE;
    }
    $clientSecret = $this->config
      ->get('client_secret');
    if (empty($clientSecret)) {
      \Drupal::logger(__METHOD__)->error('Bad config data: %missingData',
        ['%missingData' => 'client_secret']
      );
      $token_requisites = FALSE;
    }

    $scope = $this->config->get('scope');
    $accountId = $this->config->get('account_id');
    // Make token request/s.
    if ($token_requisites) {
      $loginAttempts = 0;
      $loginAttemptsMax = $this->config->get('login_attempts_max');
      while (!$token && $loginAttempts++ < $loginAttemptsMax) {
        $token = $this->requestToken($clientId, $clientSecret, $scope, $accountId);
      }
    }

    $this->state->set('marketing_cloud_token', $token);
    $this->state->set('marketing_cloud_requesting_token', FALSE);
    $expiry_time = \Drupal::time()->getCurrentTime() + $this->state->get('marketing_cloud_request_token_expire');
    $this->state->set('marketing_cloud_request_token_expire', $expiry_time);

    return $token;
  }

  /**
   * Perform the API call to request a valid token.
   *
   * @param string $clientId
   *   The marketing Cloud client ID.
   * @param string $clientSecret
   *   The Marketing Cloud secret.
   * @param string $scope
   *   The Marketing Cloud scope.
   * @param int $accountId
   *   The Marketing Cloud account id.
   *
   * @return bool|string
   *   The result of the token request, or FALSE on failure.
   */
  private function requestToken($clientId, $clientSecret, $scope = '', $accountId = 0) {
    try {
      \Drupal::logger(__METHOD__)->info('%message', ['%message' => 'Fetching a new token.']);
      $url = $this->config
        ->get('request_token_url');
      $requestVersion = $this->config
        ->get('request_token_version');

      switch ($requestVersion) {
        case 'v1':
          $url .= '/v1/requestToken';
          $formParams = [
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
          ];
          $accessToken = 'accessToken';
          break;

        case 'v2':
        default:
          $url .= '/v2/token';
          $formParams = [
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'grant_type' => 'client_credentials',
          ];
          $accessToken = 'access_token';
          break;
      }

      if ($scope) {
        $formParams['scope'] = $scope;
      }
      if ($accountId) {
        $formParams['account_id'] = $accountId;
      }

      $response = \Drupal::httpClient()->post($url, [
        'verify' => FALSE,
        'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
        'form_params' => $formParams,
      ]);
      $data = json_decode($response->getBody());
      return $data->{$accessToken};
    }
    catch (RequestException $e) {
      \Drupal::logger(__METHOD__)->error('Request exception, failed to fetch token: %error', ['%error' => $e->getMessage()]);
      return FALSE;
    }
    catch (ClientException $e) {
      \Drupal::logger(__METHOD__)->error('Client exception, failed to fetch token: %error', ['%error' => $e->getMessage()]);
      return FALSE;
    }
    catch (\Exception $e) {
      \Drupal::logger(__METHOD__)->error('Generic exception, failed to fetch token: %error', ['%error' => $e->getMessage()]);
      return FALSE;
    }
  }

}
